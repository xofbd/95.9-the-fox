#!/usr/bin/env python3
from datetime import datetime


def get_days(year, month): 
    month_to_days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

    # Consider leap year
    try:
        datetime.strptime(f"{year}-02-29", "%Y-%m-%d")
    except ValueError:
        pass
    else:
        month_to_days[1] = 29

    return range(1, month_to_days[int(month)-1] + 1)


def construct_dates(year, month, days):
    return [f"{year}-{month}-{day}" for day in days]


def main(year, month):
    days = get_days(year, month)
    dates = construct_dates(year, month, days)

    for date in dates:
        print(date)


if __name__ == "__main__":
    import sys

    main(*sys.argv[1:])  

