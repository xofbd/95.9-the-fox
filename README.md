# 90s Month on 95.9 The Fox
During the weekends of the month of February [95.9 The Fox](https://www.959thefox.com/) played 90s music. The station uses [TuneGenie](https://www.tunegenie.com/), a service that makes it easy for websites to have a media player. The TuneGenie web API can be used to pull what songs were played.

This repo uses the TuneGenie web API to create a [MongoDB](https://www.mongodb.com/) database of songs 95.9 The Fox played during the weekends of February.

## Installation
1. Sign up for a [MongoDB account](https://account.mongodb.com/account/login) and create a cluster.
1. Create a user and note the username and password.
1. Install [Python Poetry](https://python-poetry.org/) in order to properly install the project.
1. Run either `poetry install` or `make install`.
1. Determine the station and API ID values. You can get these through the developer tools most browsers have. Look under "network" and find calls to the TuneGenie API.
1. (optional) Create an `.env` file to easily store application configs. You can use `env.template` as an example.

## Running
You can download and setup the database with just `make all` or `populate-db`. There's also `drop-db` script which deletes the database.

The script `run-analysis` prints out a report, showing:
1. Number of songs played
1. Number of distinct songs played
1. Average number of plays per song
1. Most played songs
1. Most played artists
1. Most distinct songs played

## License
This project is distributed under the GNU General Public License. Please see `COPYING` for more information.
