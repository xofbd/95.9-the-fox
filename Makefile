SHELL := /bin/bash
POETRY_RUN := source .env || true && poetry run
DATA_DIR := data

.PHONY: all
all: clean install download drop-db populate-db

.PHONY: download
download: .make.install
	$(POETRY_RUN) download-song-data "2023-02-04"
	$(POETRY_RUN) download-song-data "2023-02-05"
	$(POETRY_RUN) download-song-data "2023-02-11"
	$(POETRY_RUN) download-song-data "2023-02-12"
	$(POETRY_RUN) download-song-data "2023-02-18"
	$(POETRY_RUN) download-song-data "2023-02-19"
	$(POETRY_RUN) download-song-data "2023-02-25"
	$(POETRY_RUN) download-song-data "2023-02-26"

.make.db:
	$(POETRY_RUN) populate-db
	touch $@

.PHONY: populate-db
populate-db: .make.db

.PHONY: drop-db
drop-db:
	$(POETRY_RUN) drop-db

poetry.lock: pyproject.toml
	poetry lock
	touch $@

.make.install: poetry.lock
	poetry install
	touch $@

.PHONY: install
install: .make.install

archive.tar.gz:
	tar czfv $@ $(DATA_DIR)

.PHONY: clean
clean:
	rm -f .make.*
	rm -rf src/__pycache__

.PHONY: clean-all
clean-all: clean
	rm -rf $(DATA_DIR)
