from argparse import ArgumentDefaultsHelpFormatter, ArgumentParser

from src.constants import DATABASE_DEFAULT
from src.utils import connect_to_db


def drop_db(database):
    """Drop MongoDB by deleting all collections"""
    client = connect_to_db()
    db = client[database]

    for collection in db.list_collection_names():
        db[collection].drop()

    print(f"Database {database} has been cleared")
    client.close()


def cli():
    parser = ArgumentParser(
        description="Drop MongoDB database",
        formatter_class=ArgumentDefaultsHelpFormatter
    )
    parser.add_argument(
        "--database",
        type=str,
        help="database to delete",
        default=DATABASE_DEFAULT
    )
    args = parser.parse_args()

    drop_db(args.database)
