#!/usr/bin/env python3
"""
Return number of times a song is played as well as how it ranks with other
songs.
"""
from argparse import ArgumentDefaultsHelpFormatter, ArgumentParser
import math

from src.constants import COLLECTION_DEFAULT, DATABASE_DEFAULT
from src.utils import connect_to_db


def calc_plays(collection):
    cursor = collection.aggregate([
        {
            "$group":
            {
                "_id": {"song": "$song", "artist": "$artist"},
                "count": {"$count": {}},
            },
        },
        {
            "$sort": {"count": -1}
        },
    ])

    return {
        (row["_id"]["artist"], row["_id"]["song"]): (i, row["count"])
        for i, row in enumerate(cursor)
    }


def count_plays(artist, song, plays_by_song):
    return plays_by_song[(artist, song)][1]


def calc_quantile(artist, song, plays_by_song):
    rank = plays_by_song[(artist, song)][0]

    return 1 - rank / len(plays_by_song)


def calc_num_trials(plays_by_song, collection, artist, song, threshold=0.95):
    total_plays = collection.count_documents({})
    song_freq = plays_by_song[(artist, song)][1] / total_plays

    return round(math.log(1 - threshold) / math.log(1 - song_freq))


def song_exists(artist, song, plays_by_song):
    return False if plays_by_song.get((artist, song)) is None else True


def parse_args():
    parser = ArgumentParser(
        description="Report analysis from songs in database",
        formatter_class=ArgumentDefaultsHelpFormatter
    )
    parser.add_argument(
        "--database",
        type=str,
        help="database to query",
        default=DATABASE_DEFAULT,
    )
    parser.add_argument(
        "--collection",
        type=str,
        help="collection to query",
        default=COLLECTION_DEFAULT
    )
    parser.add_argument(
        "artist",
        type=str,
        help="artist",
    )
    parser.add_argument(
        "song",
        type=str,
        help="song",
    )

    return parser.parse_args()


def main(database, collection, artist, song):
    client = connect_to_db()
    collection = client[database][collection]

    plays_by_song = calc_plays(collection)
    if not song_exists(artist, song, plays_by_song):
        print(f"Could not find {song} by {artist}")
        return
    plays = count_plays(artist, song, plays_by_song)
    quantile = calc_quantile(artist, song, plays_by_song)
    num_trials = calc_num_trials(plays_by_song, collection, artist, song)

    print(f"Stats for {song} by {artist}:")
    print(f"Total plays: {plays}")
    print(f"Percentile: {100 * quantile: .2f}%")
    print(f"Number of songs played before hearing song (95% of the time): {num_trials}")

    client.close()


def cli():
    args = parse_args()
    main(args.database, args. collection, args.artist, args.song)


if __name__ == "__main__":
    cli()
