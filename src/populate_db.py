#!/usr/bin/env python3
from argparse import ArgumentDefaultsHelpFormatter, ArgumentParser
import json
from pathlib import Path

from src.constants import COLLECTION_DEFAULT,  DATABASE_DEFAULT
from src.utils import connect_to_db


def load_documents(client, database, collection):
    """Upload JSONs files to MongoDB"""
    db = client[database]
    collection = db[collection]

    for documents in json_iter():
        if documents:
            collection.insert_many(documents)


def json_iter():
    """Generator yielding decoded JSON data"""
    for json_path in Path("data").glob("*.json"):
        print(f"Loading {json_path}")
        with json_path.open("r") as fp:
            yield json.load(fp)


def cli():
    parser = ArgumentParser(
        description="Populate MongoDB with song data",
        formatter_class=ArgumentDefaultsHelpFormatter
    )
    parser.add_argument(
        "--database",
        type=str,
        help="database to create",
        default=DATABASE_DEFAULT
    )
    parser.add_argument(
        "--collection",
        type=str,
        help="collection to create",
        default=COLLECTION_DEFAULT
    )
    args = parser.parse_args()

    client = connect_to_db()
    load_documents(client, args.database, args.collection)
    client.close()


if __name__ == "__main__":
    cli()
